﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceController : MonoBehaviour
{
    public CameraZoom cameraZoom;

    public ChangeMaterialColour changeMaterialColour;

    public SceneSwitch sceneSwitch;

    public GameObject chosenDice;

    public int chosenDiceVal;
    
    
    void Awake()
    {
        //changeMaterialColour.ColourSwitch.GetComponent<Color>();
        //chosenDice = cameraZoom.GetComponent<GameObject>();

        SetUpSingleton();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetChosenDice(int buttonPressed)
    {
        chosenDiceVal = buttonPressed - 1;
    }

    private void SetUpSingleton()
    {
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }




}
