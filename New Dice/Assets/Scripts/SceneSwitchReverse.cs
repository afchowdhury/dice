﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitchReverse : MonoBehaviour
{
    public void LoadScene(string gameMenu)
    {
        SceneManager.LoadScene(gameMenu);
    }
}
