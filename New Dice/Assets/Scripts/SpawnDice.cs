﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDice : MonoBehaviour
{
    public GameObject[] dicePrefabs;

    public Transform dicePivot;

    public Rigidbody rb;

    public GameObject spawnedDice;

    //public List<GameObject> DicePrefabsList = new List<GameObject>();
    
    // Start is called before the first frame update
    void Start()
    {
        InstantiateDice(GameObject.Find("DiceController").gameObject.GetComponent<DiceController>().chosenDiceVal);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InstantiateDice(int whichDice)
    {   
        switch (whichDice)
        {
        case 0:
            spawnedDice = Instantiate(dicePrefabs[whichDice], new Vector3(1, 12, 0), Quaternion.identity, dicePivot);
            rb = spawnedDice.GetComponent<Rigidbody>();
            rb.isKinematic = true;
            break;
        case 1:
            spawnedDice = Instantiate(dicePrefabs[whichDice], new Vector3(-2, 10, 0), Quaternion.identity, dicePivot);
            rb = spawnedDice.GetComponent<Rigidbody>();
            rb.isKinematic = true;
            break;
        case 2:
            spawnedDice = Instantiate(dicePrefabs[whichDice], new Vector3(2, 10, 0), Quaternion.identity, dicePivot);
            rb = spawnedDice.GetComponent<Rigidbody>();
            rb.isKinematic = true;
            break;
        case 3:
            spawnedDice = Instantiate(dicePrefabs[whichDice], new Vector3(0, 12, 0), Quaternion.identity, dicePivot);
            rb = spawnedDice.GetComponent<Rigidbody>();
            rb.isKinematic = true;
            break;
        case 4:
            spawnedDice = Instantiate(dicePrefabs[whichDice], new Vector3(-2, 12, 0), Quaternion.identity, dicePivot);
            rb = spawnedDice.GetComponent<Rigidbody>();
            rb.isKinematic = true;
            break;
        }
    }
}
