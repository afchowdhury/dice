﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeMaterialColour : MonoBehaviour
{
    public Color[] newColours;

    public Material myMaterial;

    // Start is called before the first frame update
    void Start()
    {
        myMaterial.color = new Color(255,255,255);
    }
    
    public void ColourSwitch(int colourid)
    {
        myMaterial.color = newColours[colourid];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
