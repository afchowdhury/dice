﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollDiceButton : MonoBehaviour
{
    public GameObject diceCP;

    public GameObject spawnedDice;
    
    // Start is called before the first frame update
    public void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ButtonClick()
    {
        spawnedDice = diceCP.transform.GetChild(0).gameObject;
        spawnedDice.GetComponent<Dice>().RollDice();
    }
}
