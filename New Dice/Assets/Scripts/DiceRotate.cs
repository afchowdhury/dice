﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceRotate : MonoBehaviour
{
    //public GameObject die4, die6, die8, die10, die12;
    public float rotationSpeedX, rotationSpeedY, rotationSpeedZ;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotationSpeedX, rotationSpeedY, rotationSpeedZ);
    }

    // public void RotateDice()
    // {
    //     transform.Rotate(rotationSpeedX, rotationSpeedY, rotationSpeedZ);
    // }
}
