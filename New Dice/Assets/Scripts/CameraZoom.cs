﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CameraZoom : MonoBehaviour
{
    public GameObject[] buttonsFaces;

    public Camera mainCamera;

    public bool isMoving;
    public Vector3 targetPosition;
    public float speed;

    public DiceController diceController;

    //public float speed = 1.0f;
    
    // Start is called before the first frame update
    void Start()
    {
        speed = 10f;
        mainCamera.transform.localPosition = new Vector3(0f, 1.1f, -10.4f);

        diceController = GameObject.Find("DiceController").gameObject.GetComponent<DiceController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(targetPosition, transform.localPosition) <= 0.05f)
        {
            isMoving = false;
        }
        
        if(isMoving)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPosition, speed * Time.deltaTime);
        }
    }
    
    public void CameraZoomDice(int buttonPressed)
    {
        switch (buttonPressed)
        {
        case 1:
            //Camera.SetActive(mainCamera);
            targetPosition = new Vector3(-6.52f, 0.91f, -2.92f);
        break;
        case 2:
            //Camera.SetActive(mainCamera);
            targetPosition = new Vector3(-2.75f, 1.1f, -2.94f);
        break;
        case 3:
            //Camera.SetActive(mainCamera);
            targetPosition = new Vector3(0.53f, 0.96f, -2.67f);
        break;
        case 4:
            //Camera.SetActive(mainCamera);
            targetPosition = new Vector3(3.53f, 0.83f, -2.48f);
        break;
        case 5:
            //Camera.SetActive(mainCamera);
            targetPosition = new Vector3(6.82f, 0.64f, -2.48f);
        break;
        // default:
        //     //Camera.SetActive(mainCamera);
        //     mainCamera.transform.position = new Vector3(0f, 1.1f, -10.4f);
        //     break;
        }
        //sets isMoving to true so that the update function can begin movement.
        isMoving = true;
        //sets current chosen dice to correct value
        diceController.SetChosenDice(buttonPressed);
    }
}
